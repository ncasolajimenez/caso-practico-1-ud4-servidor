<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>CESUR Desarrollo Web Entorno Servidor</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Tablas de Multiplicar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav me-auto mb-2 mb-md-0">
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <div class="row">
<?php

$tablas=[
"primera"=>5,
"segunda"=>13,
"tercera"=>11
];

foreach ($tablas as $key => $value) :
?>

    <div class="col-3">
      <table>
        <tr>
          <th><?=$key?> tabla</th>
        </tr>
        <tr>
          <?php for ($i=1; $i <= 10; $i++) : ?>
          <td><?=$value?>x<?=$i?>=<?php echo $value*$i; ?></td>
          <?php endfor; ?>
        </tr>
      </table>
    </div>
<?php endforeach; ?>

</div>
</div>
</body>
</html>
